# Hugh Wang (wangkongheng@gmail.com)

"""
The setup file
"""

from setuptools import setup

setup(
    name='robotCLI',
    version='0.1.0',
    entry_points={
        'console_scripts': [
            'robotCLI = robotCLI.__main__:main'
        ]
    },
)