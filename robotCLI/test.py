# Hugh Wang (wangkongheng@gmail.com)

"""
Test the robot class
"""


import unittest
from robotCLI.robot import Robot


class TestRobot(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.robot_1 = Robot('north', [0, 0])
        cls.robot_2 = Robot('south', [5, 20])
        cls.robot_3 = Robot('east', [-6, 92])
        cls.robot_4 = Robot('west', [-2, -7])
        cls.robot_5 = Robot('north', [8, -778])

    @classmethod
    def tearDownClass(cls):
        pass

    def test_calcDist(self):
        "Test calcDist function"
        print('test_calcDist')
        self.assertEqual(self.robot_1.calcDist([0, 0]), 0)
        self.assertEqual(self.robot_2.calcDist([0, 0]), 25)
        self.assertEqual(self.robot_3.calcDist([0, 0]), 98)
        self.assertEqual(self.robot_4.calcDist([0, 0]), 9)
        self.assertEqual(self.robot_5.calcDist([0, 0]), 786)

    def test_goToDest(self):
        "Test goToDest function"
        print('test_goToDest')

        self.robot_1.goToDest('F1,R1,B2,L1,B3')
        self.assertEqual([-2, -2], self.robot_1.location)

        self.robot_2.goToDest('F1000,B500')
        self.assertEqual([5, -480], self.robot_2.location)

        self.robot_3.goToDest('F6,L1,B92')
        self.assertEqual([0, 0], self.robot_3.location)

        self.robot_4.goToDest('B22,R4,L4,L1,F3')
        self.assertEqual([20, -10], self.robot_4.location)

        self.robot_5.goToDest('R3,F36,F4,L1,L1,L1,F2466')
        self.assertEqual([-32, 1688], self.robot_5.location)
