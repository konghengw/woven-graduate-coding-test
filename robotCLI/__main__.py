# Hugh Wang (wangkongheng@gmail.com)

"""
The main file
"""


import argparse
from robotCLI.robot import Robot


def main():
    parser = argparse.ArgumentParser(description="A program outputs the robot's distance from starting point")

    parser.add_argument("commands", nargs=1, type=str, help="comma-separated commands")

    args = parser.parse_args()

    r = Robot('north', [0, 0])

    if len(args.commands) != 0:
        commands = args.commands[0]
        r.goToDest(commands)
        dist = r.calcDist([0, 0])
        print("The distance is ", dist)


if __name__ == '__main__':
    main()