# Hugh Wang (wangkongheng@gmail.com)

"""
A program that receives a string of commands and will output the robot's distance from it's starting point.
"""


class Robot:
    distance = 0

    def __init__(self, direction, location):
        self.direction = direction
        self.location = location

    def goToDest(self, commandstr):
        "Make robot follow the commands"
        commands = commandstr.split(",")
        for command in commands:
            self.updateLocation(command)

    def calcDist(self, initLocation):
        "The Manhattan distance between current location and initial location"
        xy1 = self.location
        xy2 = initLocation
        return abs(xy1[0] - xy2[0]) + abs(xy1[1] - xy2[1])

    def updateLocation(self, command):
        "Update location and direction of the robot after a command"
        num = int(command[1:])
        act = command[0]
        count = 0
        while count < num:
            if self.direction == 'north':
                if act == 'F':
                    self.location[1] += 1
                elif act == 'B':
                    self.location[1] -= 1
                elif act == 'R':
                    self.direction = 'east'
                elif act == 'L':
                    self.direction = 'west'
            elif self.direction == 'south':
                if act == 'F':
                    self.location[1] -= 1
                elif act == 'B':
                    self.location[1] += 1
                elif act == 'R':
                    self.direction = 'west'
                elif act == 'L':
                    self.direction = 'east'
            elif self.direction == 'east':
                if act == 'F':
                    self.location[0] += 1
                elif act == 'B':
                    self.location[0] -= 1
                elif act == 'R':
                    self.direction = 'south'
                elif act == 'L':
                    self.direction = 'north'
            elif self.direction == 'west':
                if act == 'F':
                    self.location[0] -= 1
                elif act == 'B':
                    self.location[0] += 1
                elif act == 'R':
                    self.direction = 'north'
                elif act == 'L':
                    self.direction = 'south'
            count += 1
