# Woven Graduate Coding Test
This project creates a simpe CLI application. The application receives a string of commands as the argument, and will output the robot's distance from it's starting point.

The Robot class is the main part of the package. A robot has the attributes including its direction and location. The directions include north, south, east, and west. The (x, y) coordinate system is used to represent location. The x, y values are integers. The goToDest method has the command string as input, and updates the robot's location. The calcDist method calculates the robot's distance from it's starting point.

## Getting Started
Clone the repository onto your local directory first.

## Prerequisites
1. pip3 is used to install the application.
2. Python 3.7 is used to run the application.

## Installing
1. In the Terminal, cd to the directory woven-graduate-coding-test/
2. Type command 
    ```
    pip3 install -e .
    ```
3. "Successfully installed robotCLI" message should appear

## Running the tests
1. In the Terminal, cd to the directory woven-graduate-coding-test/
2. Type command 
    ```
    python -m unittest robotCLI.test
    ```

### Test break down
All test cases are in robotCLI/test.py, the inputs cover different kinds of scenario with respect to the robot's direction, location, and commands. The aim is to find any potential bugs in the two methods used in the application, which are calcDist and goToDest in the Robot class.

1. test_calcDist compares the expected distance with the calculated distance outputed by the calcDist method
2. test_goToDest compares the expected location with the updated location through the goToDest method

## Running the application
1. After installing the application, the command to run the application is
    ```
    robotCLI [argument]
    ```
2. The [argument] is a string of comma-separated commands
3. An example is
    ```
    robotCLI F1,R1,B2,L1,B3
    ```
4. The output is the minimum amount of distance to get back to the starting point (`4` in the example above)
